function PrescanMosaicStencilRescanNyquist4slides_20210215(varargin)
%keyboard
Errors = {};
%keyboard
dbstop if error
folderPath = varargin{1}; 

%% Prepare coordinate documentation
CoordOrganoidID = {};
CoordFieldID = {};
CoordFieldX = {};
CoordFieldY = {};

%% Load LCSB
DataInFolder = dir([folderPath, filesep, '*.mes'])
DataInFolder = struct2table(DataInFolder);
MesPath = [DataInFolder.folder, filesep, DataInFolder.name];

MetaData = f_CV8000_getChannelInfo(folderPath, MesPath);
FieldCoords = MetaData.TimeLines{:}.Fields{:};
Xvec = FieldCoords.X
Yvec = FieldCoords.Y
Xunique = unique(Xvec)
Yunique = unique(Yvec)
for i = 1:height(FieldCoords)
    FieldCoords(i,'Rows') = {find(FieldCoords{i, 'Y'} == flip(Yunique))};
    FieldCoords(i,'Cols') = {find(FieldCoords{i, 'X'} == Xunique)};
end


%Search for all '\' on the provide path
k = strfind(folderPath, '\');     
%Check if the path is valid (have at lease one '\')
m = length(k);
%Input path is valid
if (m > 0)
    %Extract folder name
    %folderName = folderPath((k(m)+1):end);
    %Get FolderInfo
    folderInfo = dir(folderPath);
    InfoTable = struct2table(folderInfo);
    Paths = table2cell(rowfun(@(a,b) {[a{:},filesep,b{:}]}, InfoTable, 'InputVariables', {'folder', 'name'}));
    InfoTable.path = Paths;
    %% Filter the actual prescan images
    ImageTableRows = cellfun(@(x) ~isempty(x), regexp(InfoTable.name, 'T\d{4}F\d{3}L\d{2}.\d{2}Z\d{2}C\d{2}.tif'));
    InfoTable = InfoTable(ImageTableRows, :);
    Wells = regexp(InfoTable.name, '.*_(.\d{2})_T\d{4}F\d{3}L\d{2}.\d{2}Z\d{2}C\d{2}.tif', 'tokens');
    InfoTable.Well = cellfun(@(x) x{:}{:}, Wells, 'UniformOutput', false);
    Fields = regexp(InfoTable.name, '.*T\d{4}F(\d{3})L\d{2}.\d{2}Z\d{2}C\d{2}.tif', 'tokens');
    InfoTable.Field = cellfun(@(x) x{:}{:}, Fields, 'UniformOutput', false);

    %% Loop over fields for each well
    Wells = unique(InfoTable.Well);
    Fields = unique(InfoTable.Field);
    ChannelCount = 1;
    %Loop thru all files
    Im = [];
    for k = 1:numel(Wells)
        InfoTableThisWell = InfoTable(strcmp(InfoTable.Well, Wells{k}),:);
        for f = 1:numel(Fields)
            rowThis = FieldCoords{FieldCoords.Field == f, 'Rows'};
            colThis = FieldCoords{FieldCoords.Field == f, 'Cols'};
            InfoTableThisField = InfoTableThisWell(strcmp(InfoTableThisWell.Field, Fields{f}),:);
            %Im = [];
            fileIDcell = {};
            counters = {};
            
            %% Load multichannel and launch outputs
            for c = 1:ChannelCount
                try
                    %Multichannel image loading
                    Im(k,rowThis,colThis,c,:, :) = imread(InfoTableThisField{c,'path'}{:});
                catch ErrorThis
                    Errors{k,f,c} = ErrorThis;
                end
            end
        end
    end
            
    % reloop to combine mask and im data
    imsize = size(Im,5,6);
    Ch1mosaicSize = [length(Yunique) * imsize(1), length(Xunique) * imsize(2)];
    Ch1mosaic = zeros([numel(Wells), Ch1mosaicSize], 'uint16');
    progress = 0;
	WellPreviews = {};
    CoordsPerWell = {};
    
    for k = 1:numel(Wells)
        CoordsPerWell{k} = zeros(size(squeeze(Ch1mosaic(k,:,:))), 'logical');
        disp('Creating mosaic view for this well')
		WellPreview = [];
        RawPreview = [];
        
        for f = 1:numel(Fields)
            rowThis = FieldCoords{FieldCoords.Field == f, 'Rows'};
            colThis = FieldCoords{FieldCoords.Field == f, 'Cols'};
            % display Ch1 only
            rrange = 1 + ((rowThis - 1) * imsize(1)) : ((rowThis - 1) * imsize(1)) + imsize(1);
            crange = 1 + ((colThis - 1) * imsize(2)) : ((colThis - 1) * imsize(2)) + imsize(2);
            Ch1mosaic(k,rrange,crange) = squeeze(Im(k,rowThis,colThis,1,:, :));% .* CorrIm; %imtool(squeeze(Ch1mosaic(1,:,:)),[])
        end
        
        %% Save raw for qc
        RawPreview = f_ImAdjust(squeeze(Ch1mosaic(k,:,:)),0.999);
        imtool(RawPreview)
        imwrite(RawPreview, [folderPath, filesep, Wells{k}, '_Raw.png'])
        
        %% Segmentation
        disp('segment organoids')   % imtool(squeeze(Ch1mosaic(k,:,:)),[])
        %OrganoidMask(k,:,:) = squeeze(Ch1mosaic(k,:,:)) > 1850;
		OrganoidMask(k,:,:) = squeeze(Ch1mosaic(k,:,:)) > 4000;
        OrganoidMask(k,:,:) = bwareafilt(squeeze(OrganoidMask(k,:,:)),[2000, 5000000]);
        OrganoidMask(k,:,:) = imclose(squeeze(OrganoidMask(k,:,:)), strel('disk', 51));
        %imtool(squeeze(OrganoidMask(k,:,:)),[])
        [OrganoidLM(k,:,:), OrganoidCount] = bwlabeln(squeeze(OrganoidMask(k,:,:)));
        
        %%%%%%%%% The Grid %%%%%%%%%%%
        if k == 1
            sizeGrid = size(squeeze(OrganoidMask(k,:,:)));
            Grid =  zeros(sizeGrid, 'logical');
            MagnificationPrescan = 4;%user input
            MagnificationRescan = 20;%user input
            MagificationRatio = MagnificationRescan / MagnificationPrescan;
            %MRnx = MagificationRatio * 20; % Good old Nyquist
            MRnx = MagificationRatio * 100; % Good old Nyquist
            StepRow = round(sizeGrid(1) / MRnx);
            StepCol = round(sizeGrid(2) / MRnx);
            MarkTicksRows = [round(StepRow/2):StepRow:sizeGrid(1)];
            MarkTicksCols = [round(StepCol/2):StepCol:sizeGrid(2)];
            for row = 1:size(MarkTicksRows, 2)
                for col = 1:size(MarkTicksCols, 2)
                    Grid(MarkTicksRows(row), MarkTicksCols(col)) = 1;
                end
            end
            % imtool(Grid,[])
        end
        %%%%%%%%% The Grid %%%%%%%%%%%
        
        for OrganoidIdx = 1: OrganoidCount
            
            OrganoidThis = squeeze(OrganoidLM(k,:,:)) == OrganoidIdx;
            OrganoidThis = OrganoidThis .* Grid;%imtool(OrganoidThis,[])
            
            %% Sampling part to be customized start
            OrganoidThisSample = find(OrganoidThis);
            OrganoidThisSampleAllTruePixels = OrganoidThisSample;
            OrganoidMaskSamplingThis = zeros(size(OrganoidLM(k,:,:)));
            for PixelIdxPerOrganoid = 1:numel(OrganoidThisSampleAllTruePixels)
                progress = progress +1;
                %% Sampling part to be customized end
                OrganoidThisSample = OrganoidThisSampleAllTruePixels(PixelIdxPerOrganoid);
                CoordOrganoidID{progress} = OrganoidIdx;
                CoordFieldID{progress} = progress;
                [CoordFieldY{progress}, CoordFieldX{progress}] = ind2sub(size(OrganoidThis), OrganoidThisSample);
                CoordWell{progress} = Wells{k};
                OrganoidMaskSamplingThis(OrganoidThisSample) = progress; % create stencil
                CoordsPerWell{k}(OrganoidThisSample) = 1; % collect coordinates for the downstream analysis
            end
            OrganoidMaskSampling(k,:,:) = OrganoidMaskSamplingThis; % imtool(squeeze(OrganoidMaskSamplingThis),[])
            %BoxThisPreview = imdilate(squeeze(OrganoidMaskSampling(k,:,:)), strel('disk', 10));
            BoxThisPreview = imdilate(squeeze(OrganoidMaskSampling(k,:,:)), strel('disk', 1));
            %imtool(BoxThisPreview,[])
            if OrganoidIdx == 1
                WellPreview = imoverlay(imadjust(uint8(squeeze(OrganoidLM(k,:,:)))), BoxThisPreview, [1 0 0]);
            else
                WellPreview = imoverlay(WellPreview, BoxThisPreview, [1 0 0]);
            end
            WellPreview = insertText(WellPreview, [CoordFieldX{progress}, CoordFieldY{progress}], ['OR', num2str(OrganoidIdx), '_F', num2str(progress)], 'FontSize',32,'BoxColor',...
            'black','BoxOpacity',0.4,'TextColor','white');
            %imtool(WellPreview)
            
            
        end
        imtool(WellPreview)
		imwrite(WellPreview, [folderPath, filesep, Wells{k}, '.png'])
		WellPreviews{k} = WellPreview;
    end
    CoordSummary = table(CoordOrganoidID', CoordFieldID', CoordFieldY', CoordFieldX', CoordWell');
    CoordSummary.Properties.VariableNames = {'OrganoidID', 'FieldID', 'FieldY', 'FieldX', 'Well'};

	%% Tabular encoding summary for later decoding
	SamplingEncodingProgress = 0;
    for k = 1:numel(Wells)
        %disp('Creating mosaic view for this well')
        InfoTableThisWell = InfoTable(strcmp(InfoTable.Well, Wells{k}),:);
        
        
        for f = 1:numel(Fields)
            InfoTableThisField = InfoTableThisWell(strcmp(InfoTableThisWell.Field, Fields{f}),:);
             try % continue in case of AF error
			 

                %% Image analysis
                %% Find the zone of SNMask that corresponds to this field
                rowThis = FieldCoords{FieldCoords.Field == f, 'Rows'};
                colThis = FieldCoords{FieldCoords.Field == f, 'Cols'};
                % display Ch1 only
                rrange = 1 + ((rowThis - 1) * imsize(1)) : ((rowThis - 1) * imsize(1)) + imsize(1);
                crange = 1 + ((colThis - 1) * imsize(2)) : ((colThis - 1) * imsize(2)) + imsize(2);
                OrganoidSampleMaskThisField = CoordsPerWell{k}(rrange, crange);% imtool(OrganoidSampleMaskThisField,[])
                Marks = logical(OrganoidSampleMaskThisField);
                %imtool(Marks,[])
                [resultYall, resultXall] = find(Marks);
                if sum(Marks(:))>0
                    channel = 1;
                    outPaths{channel} = strcat(InfoTableThisField{channel,'path'}{:}(1:end-4),'.csv');
                    %Open/Create a text file
                    %% Output into first channel only
                    if channel == 1
                        %fileIDcell{channel} = fopen(outPaths{channel},'w');
                        fileIDcell{channel} = fopen(outPaths{channel},'a');
                    end
                    %Initialze counter variable
                    counters{channel} = 1;
                    for MarkID = 1:sum(Marks(:))
						SamplingEncodingProgress = SamplingEncodingProgress + 1;
                        resultX = resultXall(MarkID);
                        resultY = resultYall(MarkID);
                        %% Outputs into first channel only
                        fprintf(fileIDcell{channel},'%d,%0.0f,%0.0f\r\n',counters{channel},resultX,resultY);
                    end
                    fclose(fileIDcell{channel});
                end
             catch
                 continue % to next field without AF error
             end
        end
    end
end
timestamp = datestr(now, 'yyyymmdd_HHMMSS');
writetable(CoordSummary, [folderPath, filesep,'Encoding.csv'])
%exit program
exit();