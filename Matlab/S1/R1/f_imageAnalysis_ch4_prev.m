function [ObjectsThisOrganoid,MAP2_Mask] = f_imageAnalysis_ch4_prev(Label,ch4,PreviewPath,Sample, ObjectsThisOrganoid,OrganoidMaskArea)
    IdentityString = num2str(Label);
    %% Previews 
    % Middle plane
    Midplane = round(size(ch4, 3)/2);
    % Scalebar
    imSize = [size(ch4, 1), size(ch4, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    %% MAP2 (ch4)
    MAP2_FT = zeros(size(ch4), 'double');

    parfor p=1:size(ch4, 3)
        MAP2_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [7,1], 0);
        %SNCAconfo_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
    end
    %vol(MAP2_FT*1000, 0, 1,'hot') % imtool(SNCA_FT(:,:,5))
    %vol(MAP2_FT*1000, 0, 1)
    %SNCAMask = SNCA_FT > 0.0025; %vol(MAP2Mask, 0, 1)
    MAP2Mask = MAP2_FT > 0.0004;
    clear 'MAP2_FT'
    MAP2Mask = bwareaopen(MAP2Mask, 50);
    MAP2Mask = MAP2Mask & OrganoidMaskArea;
    MAP2Mask =medfilt3(MAP2Mask);
    MAP2_Mask = MAP2Mask;
        
    %%MAP2 Fragmentation
    % Define structuring element for surface detection
    Conn6 = strel('sphere', 1); % 6 connectivity
    % Detect surface
    SurfaceMAP2 = MAP2_Mask & ~(imerode(MAP2_Mask, Conn6));
    %vol(SurfaceMAP2)
%%Collect and make previews of MAP2  
%Collect info MAP2   
    ObjectsThisOrganoid.MAP2MaskSum = sum(MAP2_Mask(:));
    ObjectsThisOrganoid.MAP2ByNuc = sum(MAP2_Mask(:)) /  ObjectsThisOrganoid.NucMaskSum;
    ObjectsThisOrganoid.MAP2ByNucAlive = sum(MAP2_Mask(:)) / ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.MAP2Fragmentation = sum(SurfaceMAP2(:)) /  ObjectsThisOrganoid.NucMaskSum;

%Make previews MAP2
    PreviewMAP2 = imadjust(ch4(:,:,Midplane),[0.001,0.03],[0,1]);%imtool(PreviewMAP2)
    PreviewMAP2allp = imadjust(max(ch4,[],3),[0.001,0.03],[0,1]);%imtool(PreviewMAP2allp)
    
    PreviewMAP2Area1p = imoverlay2(PreviewMAP2,MAP2_Mask(:,:,Midplane), [1 0 0]);
    PreviewMAP2Area1p = imoverlay2(PreviewMAP2Area1p, BarMask, [1 1 1]);
    PreviewMAP2Perim1p = imoverlay2(PreviewMAP2, bwperim(MAP2_Mask(:,:,Midplane)), [1 0 0]);
    PreviewMAP2Perim1p = imoverlay2(PreviewMAP2Perim1p, BarMask, [1 1 1]);
    
    PreviewMAP2AreaMIP = imoverlay2(PreviewMAP2allp,max(MAP2_Mask,[],3), [1 0 0]);
    PreviewMAP2AreaMIP = imoverlay2(PreviewMAP2AreaMIP, BarMask, [1 1 1]);
    PreviewMAP2MIPerimMIP = imoverlay2(PreviewMAP2allp, bwperim(max(MAP2_Mask,[],3)), [1 0 0]);
    PreviewMAP2MIPerimMIP = imoverlay2(PreviewMAP2MIPerimMIP, BarMask, [1 1 1]);
       
    imwrite(PreviewMAP2, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'MAP2_raw_1p', '.png'])
    imwrite(PreviewMAP2allp, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'MAP2_raw_MIP', '.png'])
    imwrite(PreviewMAP2Area1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'MAP2_Area_1p', '.png'])
    imwrite(PreviewMAP2Perim1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'MAP2_Perim_1p', '.png'])
    imwrite(PreviewMAP2AreaMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'MAP2_Area_MIP', '.png'])
    imwrite(PreviewMAP2MIPerimMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'MAP2_Perim_MIP', '.png'])
end

