function [ObjectsThisOrganoid] = f_imageAnalysis_Colo_prev(Label,ch1,ch2,ch3,ch4,PreviewPath,Sample,ObjectsThisOrganoid,C19Mask,THMask,MAP2_Mask)

    IdentityString = num2str(Label);
    %% Previews 
    % Middle plane
    Midplane = round(size(ch3, 3)/2);
    % Scalebar
    imSize = [size(ch3, 1), size(ch3, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
%% Colocalization Masks
ColoC19andTH = C19Mask & THMask;
ColoC19andMAP2 = MAP2_Mask & C19Mask;
ColoTHandMAP2 = MAP2_Mask & THMask;
ColoTHMAP2C19 = MAP2_Mask & THMask & C19Mask;

 %% Previews
    RGB_C19_1p = cat (3,  imadjust(ch4(:,:,Midplane),[0.001,0.03],[0,1]), imadjust(ch2(:,:,Midplane),[0,0.03],[0,1]), imadjust(ch1(:,:,Midplane),[0,0.05],[0,1]));
    RGB_C19_1p = imoverlay2(RGB_C19_1p, BarMask, [1 1 1]);
    RGB_C19_all = cat (3, imadjust(max(ch4,[],3),[0.001,0.03],[0,1]), imadjust(max(ch2,[],3),[0,0.03],[0,1]), imadjust(max(ch1,[],3),[0,0.05],[0,1]));
    RGB_C19_all = imoverlay2(RGB_C19_all, BarMask, [1 1 1]);
    RGB_THC19_1p = cat (3,  imadjust(ch3(:,:,Midplane),[0,0.025],[0,1]),imadjust(ch2(:,:,Midplane),[0,0.03],[0,1]), imadjust(ch1(:,:,Midplane),[0,0.05],[0,1]));
    RGB_THC19_1p = imoverlay2(RGB_THC19_1p, BarMask, [1 1 1]);
    RGB_THC19_all = cat (3,  imadjust(max(ch3,[],3),[0,0.025],[0,1]),imadjust(max(ch2,[],3),[0,0.03],[0,1]), imadjust(max(ch1,[],3),[0,0.05],[0,1]));
    RGB_THC19_all = imoverlay2(RGB_THC19_all, BarMask, [1 1 1]);
    RGB_TH_1p = cat (3,  imadjust(ch4(:,:,Midplane),[0.001,0.02],[0,1]), imadjust(ch3(:,:,Midplane),[0,0.03],[0,1]), imadjust(ch1(:,:,Midplane),[0,0.08],[0,1]));
    RGB_TH_1p = imoverlay2(RGB_TH_1p, BarMask, [1 1 1]);
    RGB_TH_all = cat (3,  imadjust(max(ch4,[],3),[0.001,0.02],[0,1]), imadjust(max(ch3,[],3),[0,0.03],[0,1]), imadjust(max(ch1,[],3),[0,0.08],[0,1]));
    RGB_TH_all = imoverlay2(RGB_TH_all, BarMask, [1 1 1]);
    RGB_all_1p = cat (3,  imadjust(ch4(:,:,Midplane),[0.001,0.03],[0,1]), imadjust(ch3(:,:,Midplane),[0,0.025],[0,1]), imadjust(ch2(:,:,Midplane),[0.001,0.01],[0,1]));
    RGB_all_1p = imoverlay2(RGB_all_1p, BarMask, [1 1 1]);
    RGB_all = cat (3,  imadjust(max(ch4,[],3),[0.001,0.03],[0,1]), imadjust(max(ch3,[],3),[0,0.025],[0,1]), imadjust(max(ch2,[],3),[0.001,0.01],[0,1]));
    RGB_all = imoverlay2(RGB_all, BarMask, [1 1 1]);
    %imtool(RGB_C19_1p)
    %imtool(RGB_C19_all)
    %imtool(RGB_TH_1p)
    %imtool(RGB_TH_all)
    %imtool(RGB_THC19_1p)
    %imtool(RGB_THC19_all)
    %imtool(RGB_all_1p)
    %imtool(RGB_all)
    
    imwrite(RGB_C19_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_C19_1p', '.png'])
    imwrite(RGB_C19_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_C19_all', '.png'])
    imwrite(RGB_TH_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_TH_1p', '.png'])
    imwrite(RGB_TH_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_TH_all', '.png'])
    imwrite(RGB_THC19_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_THC19_1p', '.png'])
    imwrite(RGB_THC19_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_THC19_all', '.png'])
    imwrite(RGB_all_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_all_1p', '.png'])
    imwrite(RGB_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_all', '.png'])
    
     %% Feature extraction
    ObjectsThisOrganoid.ColoC19andTH = sum(ColoC19andTH(:));
    ObjectsThisOrganoid.ColoC19andMAP2 = sum(ColoC19andMAP2(:));
    ObjectsThisOrganoid.ColoTHandMAP2 = sum(ColoTHandMAP2(:));
    ObjectsThisOrganoid.ColoTHMAP2C19= sum(ColoTHMAP2C19(:));
    ObjectsThisOrganoid.ColoC19andTHNormNeu = sum(ColoC19andTH(:))/ sum(MAP2_Mask(:));
    ObjectsThisOrganoid.ColoC19andMAP2NormNeu = sum(ColoC19andMAP2(:))/ sum(MAP2_Mask(:));
    ObjectsThisOrganoid.ColoC19andTHNormNuc = sum(ColoC19andTH(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoC19andMAP2NormNuc = sum(ColoC19andMAP2(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoTHandMAP2NormNeu = sum(ColoTHandMAP2(:))/ sum(MAP2_Mask(:));
    ObjectsThisOrganoid.ColoTHandMAP2NormNuc = sum(ColoTHandMAP2(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoTHMAP2C19NormNeu = sum(ColoTHMAP2C19(:))/ sum(MAP2_Mask(:));
    ObjectsThisOrganoid.ColoTHMAP2C19NormNuc = sum(ColoTHMAP2C19(:))/ ObjectsThisOrganoid.NucMaskAlive;
    
    %% Colocalization Previews
    if sum(ColoC19andTH(:))> 0
      RGB_ColoC19andTHper = imoverlay(RGB_THC19_all, bwperim(max(ColoC19andTH,[],3)), [1 1 1]); 
      RGB_ColoC19andTHarea = imoverlay(RGB_THC19_all, max(ColoC19andTH,[],3), [1 1 1]); 
      filename_RGB_ColoC19andTHper = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19THAllper', '.png'];
      filename_RGB_ColoC19andTHar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19THAllar', '.png'];
      imwrite(RGB_ColoC19andTHper,filename_RGB_ColoC19andTHper);
      imwrite(RGB_ColoC19andTHarea,filename_RGB_ColoC19andTHar);
    else
    end
    if sum(ColoC19andMAP2(:))> 0
      RGB_ColoC19andMAP2per = imoverlay(RGB_C19_all, bwperim(max(ColoC19andMAP2,[],3)), [1 1 1]); 
      RGB_ColoC19andMAP2area = imoverlay(RGB_C19_all, max(ColoC19andMAP2,[],3), [1 1 1]); 
      filename_RGB_ColoC19andMAP2per = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19MAPAllper', '.png'];
      filename_RGB_ColoC19andMAP2ar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19MAPAllar', '.png'];
      imwrite(RGB_ColoC19andMAP2per,filename_RGB_ColoC19andMAP2per);
      imwrite(RGB_ColoC19andMAP2area,filename_RGB_ColoC19andMAP2ar);
    else
    end
    if sum(ColoTHandMAP2(:))> 0
      RGB_ColoTHandMAP2per = imoverlay(RGB_TH_all, bwperim(max(ColoTHandMAP2,[],3)), [1 1 1]); 
      RGB_ColoTHandMAP2area = imoverlay(RGB_TH_all, max(ColoTHandMAP2,[],3), [1 1 1]); 
      filename_RGB_ColoTHandMAP2per = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoTHMAPAllper', '.png'];
      filename_RGB_ColoTHandMAP2ar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoTHMAPAllar', '.png'];
      imwrite(RGB_ColoTHandMAP2per,filename_RGB_ColoTHandMAP2per);
      imwrite(RGB_ColoTHandMAP2area,filename_RGB_ColoTHandMAP2ar);
    else
    end
    if sum(ColoTHMAP2C19(:))> 0
      RGB_ColoTHMAP2C19per = imoverlay(RGB_all, bwperim(max(ColoTHMAP2C19,[],3)), [1 1 1]); 
      RGB_ColoTHMAP2C19area = imoverlay(RGB_all, max(ColoTHMAP2C19,[],3), [1 1 1]); 
      filename_RGB_ColoTHMAP2C19per = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoAllper', '.png'];
      filename_RGB_ColoTHMAP2C19ar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoAllar', '.png'];
      imwrite(RGB_ColoTHMAP2C19per,filename_RGB_ColoTHMAP2C19per);
      imwrite(RGB_ColoTHMAP2C19area,filename_RGB_ColoTHMAP2C19ar);
    else
    end
end

