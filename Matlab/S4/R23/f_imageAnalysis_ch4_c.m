function [ObjectsThisOrganoid,GFAPMask] = f_imageAnalysis_ch4_c(Label,ch4,PreviewPath,Sample,ObjectsThisOrganoid,OrganoidMaskArea)

    IdentityString = num2str(Label);
    %% Previews 
    % Middle plane
    Midplane = round(size(ch4, 3)/2);
    % Scalebar
    imSize = [size(ch4, 1), size(ch4, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    
    %% GFAP (ch4)
    GFAP_FT = zeros(size(ch4), 'double');

    parfor p=1:size(ch4, 3)
        %GFAP_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [7,1], 0);
        GFAP_FT(:,:,p) = f_LPF_by_FFT(ch4(:,:,p), 'Butterworth', [15,1], 0);
    end
    %vol(GFAP_FT*100,0,1,'hot')
    GFAPMask = GFAP_FT > 0.004;% vol(GFAPMask)
    clear 'GFAP_FT'
    
    GFAPMask = bwareaopen(GFAPMask, 50);
    GFAPMask = GFAPMask & OrganoidMaskArea;
    GFAPMask = medfilt3(GFAPMask);
    %vol(GFAPMask)   
 %%GFAP Fragmentation
    % Define structuring element for surface detection
    Conn6 = strel('sphere', 1); % 6 connectivity
    % Detect surface
    SurfaceGFAP = GFAPMask & ~(imerode(GFAPMask, Conn6));
    %vol(SurfaceGFAP)
%%Collect and make previews of GFAP
    ObjectsThisOrganoid.GFAPMaskSum = sum(GFAPMask(:));
    ObjectsThisOrganoid.GFAPByNuc = sum(GFAPMask(:)) /  ObjectsThisOrganoid.NucMaskSum;
    ObjectsThisOrganoid.GFAPByNucAlive = sum(GFAPMask(:)) /  ObjectsThisOrganoid.NucMaskSum;
	ObjectsThisOrganoid.GFAPFragmentation = sum(SurfaceGFAP(:)) /  ObjectsThisOrganoid.NucMaskSum;
    ObjectsThisOrganoid.GFAPByS100 = ObjectsThisOrganoid.GFAPMaskSum / ObjectsThisOrganoid.S100MaskSum;
 
%Make previews GFAP    
    PreviewGFAP = imadjust(ch4(:,:,Midplane),[0.002,0.02],[0,1]);%imtool(PreviewGFAP)
    PreviewGFAPallp = imadjust(max(ch4,[],3),[0.002,0.02],[0,1]);%imtool(PreviewGFAPallp)
    
    PreviewGFAPArea1p = imoverlay2(PreviewGFAP,GFAPMask(:,:,Midplane), [1 0 0]);
    PreviewGFAPArea1p = imoverlay2(PreviewGFAPArea1p, BarMask, [1 1 1]);
	PreviewGFAPPerim1p = imoverlay2(PreviewGFAP, bwperim(GFAPMask(:,:,Midplane)), [1 0 0]);
    PreviewGFAPPerim1p = imoverlay2(PreviewGFAPPerim1p, BarMask, [1 1 1]);
   
    PreviewGFAPAreaMIP = imoverlay2(PreviewGFAPallp,max(GFAPMask,[],3), [1 0 0]);
    PreviewGFAPAreaMIP = imoverlay2(PreviewGFAPAreaMIP, BarMask, [1 1 1]);
	PreviewGFAPPerimMIP = imoverlay2(PreviewGFAPallp, bwperim(max(GFAPMask,[],3)), [1 0 0]);
    PreviewGFAPPerimMIP = imoverlay2(PreviewGFAPPerimMIP, BarMask, [1 1 1]);
    
   
    imwrite(PreviewGFAP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'GFAP_raw_MIP', '.png'])
    imwrite(PreviewGFAPallp, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'GFAP_raw_1p', '.png'])
    imwrite(PreviewGFAPArea1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'GFAP_Area_1p', '.png'])
    imwrite(PreviewGFAPPerim1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'GFAP_Perim_1p', '.png'])
    imwrite(PreviewGFAPAreaMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'GFAP_Area_MIP', '.png'])
    imwrite(PreviewGFAPPerimMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'GFAP_Perim_MIP', '.png'])
   
end

