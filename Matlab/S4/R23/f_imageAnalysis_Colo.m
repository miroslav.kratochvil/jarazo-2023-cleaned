function [ObjectsThisOrganoid] = f_imageAnalysis_Colo(Label,ch1,ch2,ch3,ch4,PreviewPath,Sample,ObjectsThisOrganoid,C19Mask,GFAPMask,S100Mask)

    IdentityString = num2str(Label);
    %% Previews 
    % Middle plane
    Midplane = round(size(ch3, 3)/2);
    % Scalebar
    imSize = [size(ch3, 1), size(ch3, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
%% Colocalization Masks
ColoC19andGFAP = C19Mask & GFAPMask;
ColoC19andS100 = S100Mask & C19Mask;
ColoGFAPandS100 = S100Mask & GFAPMask;
ColoGFAPS100C19 = S100Mask & GFAPMask & C19Mask;

 %% Previews
    RGB_C19_1p = cat (3,  imadjust(ch3(:,:,Midplane),[0,0.07],[0,1]), imadjust(ch2(:,:,Midplane),[0.002,0.03],[0,1]), imadjust(ch1(:,:,Midplane),[0,0.08],[0,1]));
    RGB_C19_1p = imoverlay2(RGB_C19_1p, BarMask, [1 1 1]);
    RGB_C19_all = cat (3, imadjust(max(ch3,[],3),[0,0.07],[0,1]), imadjust(max(ch2,[],3),[0.002,0.03],[0,1]), imadjust(max(ch1,[],3),[0,0.08],[0,1]));
    RGB_C19_all = imoverlay2(RGB_C19_all, BarMask, [1 1 1]);
    RGB_GFAPC19_1p = cat (3,  imadjust(ch3(:,:,Midplane),[0,0.07],[0,1]),imadjust(ch4(:,:,Midplane),[0.002,0.02],[0,1]), imadjust(ch1(:,:,Midplane),[0,0.08],[0,1]));
    RGB_GFAPC19_1p = imoverlay2(RGB_GFAPC19_1p, BarMask, [1 1 1]);
    RGB_GFAPC19_all = cat (3,  imadjust(max(ch3,[],3),[0,0.07],[0,1]),imadjust(max(ch4,[],3),[0.002,0.02],[0,1]), imadjust(max(ch1,[],3),[0,0.08],[0,1]));
    RGB_GFAPC19_all = imoverlay2(RGB_GFAPC19_all, BarMask, [1 1 1]);
    RGB_GFAP_1p = cat (3,  imadjust(ch4(:,:,Midplane),[0.002,0.02],[0,1]), imadjust(ch2(:,:,Midplane),[0.002,0.03],[0,1]), imadjust(ch1(:,:,Midplane),[0,0.08],[0,1]));
    RGB_GFAP_1p = imoverlay2(RGB_GFAP_1p, BarMask, [1 1 1]);
    RGB_GFAP_all = cat (3,  imadjust(max(ch4,[],3),[0.002,0.02],[0,1]), imadjust(max(ch2,[],3),[0.002,0.03],[0,1]), imadjust(max(ch1,[],3),[0,0.08],[0,1]));
    RGB_GFAP_all = imoverlay2(RGB_GFAP_all, BarMask, [1 1 1]);
    RGB_all_1p = cat (3,  imadjust(ch4(:,:,Midplane),[0.002,0.02],[0,1]), imadjust(ch2(:,:,Midplane),[0.002,0.03],[0,1]), imadjust(ch3(:,:,Midplane),[0,0.07],[0,1]));
    RGB_all_1p = imoverlay2(RGB_all_1p, BarMask, [1 1 1]);
    RGB_all = cat (3,  imadjust(max(ch4,[],3),[0.002,0.02],[0,1]), imadjust(max(ch2,[],3),[0.002,0.03],[0,1]), imadjust(max(ch3,[],3),[0,0.07],[0,1]));
    RGB_all = imoverlay2(RGB_all, BarMask, [1 1 1]);
    %imtool(RGB_C19_1p)
    %imtool(RGB_C19_all)
    %imtool(RGB_GFAP_1p)
    %imtool(RGB_GFAP_all)
    %imtool(RGB_GFAPC19_1p)
    %imtool(RGB_GFAPC19_all)
    %imtool(RGB_Colo_all)
    
    imwrite(RGB_C19_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_C19_1p', '.png'])
    imwrite(RGB_C19_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_C19_all', '.png'])
    imwrite(RGB_GFAP_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_GFAP_1p', '.png'])
    imwrite(RGB_GFAP_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_GFAP_all', '.png'])
    imwrite(RGB_GFAPC19_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_GFAPC19_1p', '.png'])
    imwrite(RGB_GFAPC19_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_GFAPC19_all', '.png'])
    imwrite(RGB_all_1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_all_1p', '.png'])
    imwrite(RGB_all, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'RGB_all', '.png'])
     %% Feature extraction
    ObjectsThisOrganoid.ColoC19andGFAP = sum(ColoC19andGFAP(:));
    ObjectsThisOrganoid.ColoC19andS100 = sum(ColoC19andS100(:));
    ObjectsThisOrganoid.ColoGFAPandS100 = sum(ColoGFAPandS100(:));
    ObjectsThisOrganoid.ColoGFAPS100C19 = sum(ColoGFAPS100C19(:));
    ObjectsThisOrganoid.ColoC19andGFAPNormS100 = sum(ColoC19andGFAP(:))/ sum(S100Mask(:));
    ObjectsThisOrganoid.ColoC19andS100NormS100 = sum(ColoC19andS100(:))/ sum(S100Mask(:));
    ObjectsThisOrganoid.ColoC19andGFAPNormNuc = sum(ColoC19andGFAP(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoC19andS100NormNuc = sum(ColoC19andS100(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoGFAPandS100NormS100 = sum(ColoGFAPandS100(:))/ sum(S100Mask(:));
    ObjectsThisOrganoid.ColoGFAPandS100NormNuc = sum(ColoGFAPandS100(:))/ ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.ColoGFAPS100C19NormS100 = sum(ColoGFAPS100C19(:))/ sum(S100Mask(:));
    ObjectsThisOrganoid.ColoGFAPS100C19NormNuc = sum(ColoGFAPS100C19(:))/ ObjectsThisOrganoid.NucMaskAlive;
    %% Colocalization Previews
    if sum(ColoC19andGFAP(:))> 0
      RGB_ColoC19andGFAPper = imoverlay(RGB_GFAPC19_all, bwperim(max(ColoC19andGFAP,[],3)), [1 1 1]); 
      RGB_ColoC19andGFAParea = imoverlay(RGB_GFAPC19_all, max(ColoC19andGFAP,[],3), [1 1 1]); 
      filename_RGB_ColoC19andGFAPper = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19GFAPAllper', '.png'];
      filename_RGB_ColoC19andGFAPar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19GFAPAllar', '.png'];
      imwrite(RGB_ColoC19andGFAPper,filename_RGB_ColoC19andGFAPper);
      imwrite(RGB_ColoC19andGFAParea,filename_RGB_ColoC19andGFAPar);
    else
    end
    if sum(ColoC19andS100(:))> 0
      RGB_ColoC19andS100per = imoverlay(RGB_C19_all, bwperim(max(ColoC19andS100,[],3)), [1 1 1]); 
      RGB_ColoC19andS100area = imoverlay(RGB_C19_all, max(ColoC19andS100,[],3), [1 1 1]); 
      filename_RGB_ColoC19andS100per = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19S100Allper', '.png'];
      filename_RGB_ColoC19andS100ar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoC19S100Allar', '.png'];
      imwrite(RGB_ColoC19andS100per,filename_RGB_ColoC19andS100per);
      imwrite(RGB_ColoC19andS100area,filename_RGB_ColoC19andS100ar);
    else
    end
    if sum(ColoGFAPandS100(:))> 0
      RGB_ColoGFAPandS100per = imoverlay(RGB_GFAP_all, bwperim(max(ColoGFAPandS100,[],3)), [1 1 1]); 
      RGB_ColoGFAPandS100area = imoverlay(RGB_GFAP_all, max(ColoGFAPandS100,[],3), [1 1 1]); 
      filename_RGB_ColoGFAPandS100per = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoGFAPS100Allper', '.png'];
      filename_RGB_ColoGFAPandS100ar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoGFAPS100Allar', '.png'];
      imwrite(RGB_ColoGFAPandS100per,filename_RGB_ColoGFAPandS100per);
      imwrite(RGB_ColoGFAPandS100area,filename_RGB_ColoGFAPandS100ar);
    else
    end
    if sum(ColoGFAPS100C19(:))> 0
      RGB_ColoGFAPS100C19per = imoverlay(RGB_all, bwperim(max(ColoGFAPS100C19,[],3)), [1 1 1]); 
      RGB_ColoGFAPS100C19area = imoverlay(RGB_all, max(ColoGFAPS100C19,[],3), [1 1 1]); 
      filename_RGB_ColoGFAPS100C19per = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoAllper', '.png'];
      filename_RGB_ColoGFAPS100C19ar = [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'ColoAllar', '.png'];
      imwrite(RGB_ColoGFAPS100C19per,filename_RGB_ColoGFAPS100C19per);
      imwrite(RGB_ColoGFAPS100C19area,filename_RGB_ColoGFAPS100C19ar);
    else
    end
end

