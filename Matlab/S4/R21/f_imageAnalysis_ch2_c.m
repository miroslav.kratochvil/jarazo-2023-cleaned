function [ObjectsThisOrganoid,S100Mask] = f_imageAnalysis_ch2_c(Label,ch2,PreviewPath,Sample, ObjectsThisOrganoid,OrganoidMaskArea)
    IdentityString = num2str(Label);
    %% Previews 
    % Middle plane
    Midplane = round(size(ch2, 3)/2);
    % Scalebar
    imSize = [size(ch2, 1), size(ch2, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    %% SNCA (ch2)
    S100_FT = zeros(size(ch2), 'double');

    parfor p=1:size(ch2, 3)
        S100_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [7,1], 0);
        %S100confo_FT(:,:,p) = f_LPF_by_FFT(ch2(:,:,p), 'Butterworth', [15,1], 0);
    end
    %vol(S100_FT*100, 0, 1,'hot') % imtool(S100_FT(:,:,5))
    %vol(S100_FT*1000, 0, 1)
    %S100Mask = S100_FT > 0.0025; %vol(S100Mask, 0, 1)
    S100Mask = S100_FT > 0.003;
    clear 'S100_FT'
    S100Mask = bwareaopen(S100Mask, 50);
    S100Mask = S100Mask & OrganoidMaskArea;
    S100Mask =medfilt3(S100Mask);
    
        
    %%S100 Fragmentation
    % Define structuring element for surface detection
    Conn6 = strel('sphere', 1); % 6 connectivity
    % Detect surface
    SurfaceS100 = S100Mask & ~(imerode(S100Mask, Conn6));
    %vol(SurfaceS100)
%%Collect and make previews of S100  
%Collect info S100   
    ObjectsThisOrganoid.S100MaskSum = sum(S100Mask(:));
    ObjectsThisOrganoid.S100ByNuc = sum(S100Mask(:)) /  ObjectsThisOrganoid.NucMaskSum;
    ObjectsThisOrganoid.S100ByNucAlive = sum(S100Mask(:)) / ObjectsThisOrganoid.NucMaskAlive;
    ObjectsThisOrganoid.S100Fragmentation = sum(SurfaceS100(:)) /  ObjectsThisOrganoid.NucMaskSum;

%Make previews S100
    PreviewS100 = imadjust(ch2(:,:,Midplane),[0.002,0.03],[0,1]);%imtool(PreviewS100)
    PreviewS100allp = imadjust(max(ch2,[],3),[0.002,0.03],[0,1]);%imtool(PreviewS100allp)
    
    PreviewS100Area1p = imoverlay2(PreviewS100,S100Mask(:,:,Midplane), [1 0 0]);
    PreviewS100Area1p = imoverlay2(PreviewS100Area1p, BarMask, [1 1 1]);
    PreviewS100Perim1p = imoverlay2(PreviewS100, bwperim(S100Mask(:,:,Midplane)), [1 0 0]);
    PreviewS100Perim1p = imoverlay2(PreviewS100Perim1p, BarMask, [1 1 1]);
    
    PreviewS100AreaMIP = imoverlay2(PreviewS100allp,max(S100Mask,[],3), [1 0 0]);
    PreviewS100AreaMIP = imoverlay2(PreviewS100AreaMIP, BarMask, [1 1 1]);
    PreviewS100MIPerimMIP = imoverlay2(PreviewS100allp, bwperim(max(S100Mask,[],3)), [1 0 0]);
    PreviewS100MIPerimMIP = imoverlay2(PreviewS100MIPerimMIP, BarMask, [1 1 1]);
       
    imwrite(PreviewS100, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'S100_raw_1p', '.png'])
    imwrite(PreviewS100allp, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'S100_raw_MIP', '.png'])
    imwrite(PreviewS100Area1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'S100_Area_1p', '.png'])
    imwrite(PreviewS100Perim1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'S100_Perim_1p', '.png'])
    imwrite(PreviewS100AreaMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'S100_Area_MIP', '.png'])
    imwrite(PreviewS100MIPerimMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'S100_Perim_MIP', '.png'])
end

