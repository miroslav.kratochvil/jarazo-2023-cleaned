function [ObjectsThisOrganoid,OrganoidMaskArea] = f_imageAnalysis_ch1(Label, ch1, PreviewPath,Sample, ThisWellString)

    %% Constant feature extraction
    ObjectsThisOrganoid = table();
    ObjectsThisOrganoid.LabelIdx = Label;
    ObjectsThisOrganoid.AreaName = convertCharsToStrings(Sample);
    ObjectsThisOrganoid.Slide = convertCharsToStrings(ThisWellString);
    IdentityString = num2str(Label);
     %% Previews 
    % Middle plane
    Midplane = round(size(ch1, 3)/2);
    % Scalebar
    imSize = [size(ch1, 1), size(ch1, 2)];
    pixelSize = 0.32393102760889064;
    [BarMask, ~] = f_barMask(200, pixelSize , imSize, imSize(1)-200, 200, 25);
    %it(BarMask)
    
    %% Segment nuclei
    %vol(ch1, 0, 6000)
    %ch1BlurSmall = imfilter(double(ch1), fspecial('gaussian', 21, 1), 'symmetric');%vol(ch3BlurSmall)
    %ch1BlurBig = imfilter(double(ch1), fspecial('gaussian', 21, 3), 'symmetric');%vol(ch3BlurBig) %%kind of flatfield corraction, to account for different bk in the pic
    %ch1DoG = ch1BlurSmall - ch1BlurBig; %vol(ch3DoG, 0, 2000, 'hot')
    ch1DoG = imfilter(ch1, fspecial('gaussian', 101, 3) - fspecial('gaussian', 101, 7), 'symmetric');%vol(ch1DoG, 0, 10)
    NucleiMask = ch1DoG > 50; %vol(NucleiMask)
    clear 'ch1DoG'
    NucleiMask = bwareaopen(NucleiMask, 300);%vol(NucleiMask)
      
    %% Organoid Mask (to remove the autofluorescence background)
    OrganoidMaskArea = imdilate(imdilate(NucleiMask, strel('disk', 40)), strel('sphere',5));
    OrganoidMaskArea = bwareaopen(OrganoidMaskArea, 100000);%vol(OrganoidMaskArea)
      
    %% Select dead v alive nuclei
    ch1LP = imfilter(ch1, fspecial('gaussian', 11, 1), 'symmetric');%vol(ch1LP, 0, 5000, 'hot')
    NucMaskHigh =  (ch1LP > 3500) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    clear 'ch1LP'
    NucMaskAlive = NucleiMask & ~NucMaskHigh & OrganoidMaskArea; % vol(NucMaskAlive)
    
%%Collect and make previews of Nuclei and Organoid area
    %Collect info
    ObjectsThisOrganoid.OrganoidMask = sum(OrganoidMaskArea(:));
    ObjectsThisOrganoid.NucMaskSum = sum(NucleiMask(:));
    ObjectsThisOrganoid.NucMaskDead = sum(NucMaskHigh(:));
    ObjectsThisOrganoid.NucMaskAlive = sum(NucMaskAlive(:));

    %Make previews
    PreviewNucAll = imadjust(ch1(:,:,Midplane),[0,0.08],[0,1]);%imtool(PreviewNucAll)
    PreviewNucAllallp = imadjust(max(ch1,[],3),[0,0.08],[0,1]);%imtool(PreviewNucAllallp)
    
    PreviewNucAllArea1p = imoverlay2(PreviewNucAll,NucleiMask(:,:,Midplane), [1 0 0]);
    PreviewNucAllArea1p = imoverlay2(PreviewNucAllArea1p, BarMask, [1 1 1]);
    PreviewNucAllPerim1p = imoverlay2(PreviewNucAll, bwperim(NucleiMask(:,:,Midplane)), [1 0 0]);
    PreviewNucAllPerim1p = imoverlay2(PreviewNucAllPerim1p, BarMask, [1 1 1]);
    
   
    PreviewNucDeadArea1p = imoverlay2(imadjust(ch1(:,:,Midplane),[0,0.08],[0,1]),NucMaskHigh(:,:,Midplane), [1 0 0]);
    PreviewNucDeadArea1p = imoverlay2(PreviewNucDeadArea1p, BarMask, [1 1 1]);
    PreviewNucDeadPerim1p = imoverlay2(imadjust(ch1(:,:,Midplane),[0,0.08],[0,1]), bwperim(NucMaskHigh(:,:,Midplane)), [1 0 0]);
    PreviewNucDeadPerim1p = imoverlay2(PreviewNucDeadPerim1p, BarMask, [1 1 1]);
    
    PreviewNucAllAreaMIP = imoverlay2(PreviewNucAllallp,max(NucleiMask,[],3), [1 0 0]);
    PreviewNucAllAreaMIP = imoverlay2(PreviewNucAllAreaMIP, BarMask, [1 1 1]);
    PreviewNucAllPerimMIP = imoverlay2(PreviewNucAllallp, bwperim(max(NucleiMask,[],3)), [1 0 0]);
    PreviewNucAllPerimMIP = imoverlay2(PreviewNucAllPerimMIP, BarMask, [1 1 1]);
    
   
    PreviewNucDeadAreaMIP = imoverlay2(imadjust(max(ch1,[],3),[0,0.08],[0,1]),max(NucMaskHigh,[],3), [1 0 0]);
    PreviewNucDeadAreaMIP = imoverlay2(PreviewNucDeadAreaMIP, BarMask, [1 1 1]);
    PreviewNucDeadPerimMIP = imoverlay2(imadjust(max(ch1,[],3),[0,0.08],[0,1]), bwperim(max(NucMaskHigh,[],3)), [1 0 0]);
    PreviewNucDeadPerimMIP = imoverlay2(PreviewNucDeadPerimMIP, BarMask, [1 1 1]);
        
    imwrite(PreviewNucAll, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucAll_raw', '.png'])
    imwrite(PreviewNucAllallp, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucAll_raw_MIP', '.png'])
    imwrite(PreviewNucAllArea1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucAll_Area_1p', '.png'])
    imwrite(PreviewNucAllPerim1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucAll_Perim_1p', '.png'])
    imwrite(PreviewNucDeadArea1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucDead_Area_1p', '.png'])
    imwrite(PreviewNucDeadPerim1p, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucDead_Perim_1p', '.png'])
    imwrite(PreviewNucAllAreaMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucAll_Area_MIP', '.png'])
    imwrite(PreviewNucAllPerimMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucAll_Perim_MIP', '.png'])
    imwrite(PreviewNucDeadAreaMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucDead_Area_MIP', '.png'])
    imwrite(PreviewNucDeadPerimMIP, [PreviewPath, filesep, Sample ,'_', IdentityString,'_' ,'NucDead_Perim_MIP', '.png']) 
end

