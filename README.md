Figure generation script key
R - Image analysis results

	MBO_stats_plots_un_SF.R -> Fig. 1c and Fig. 2b
	MBO_stats_plots_c19ratios.R -> Fig. 2e
	MBO_stats_plots_c19ratios_densitogram.R -> Fig. 2d

R - Gene expression

	Enrichment.R -> Fig. 3c, Fig. 3d, Fig. S4b, Fig. S5a, and Fig. S5b
	Gene_expression_bar_graph.R -> Fig. 3a
	GREAT_plots.R -> Fig. 3e and Fig. S6
	Heatmaps_boxplots_selected_genes.R -> Fig. S2b, Fig. S4a, and Fig. S5c
	Volcano_plot.R -> Fig. S2a
	Non-coding_RNA_filtering.R -> used to select the non-coding RNA to transform them to BED for using GREAT

Matlab - Image acquisition

	Prescan4xMosaicRescan20x_20210727.m -> used in SearchFirst modality of the Wako Software for imaging in the Yokogawa CV8000

Matlab - Image analysis

	Scripts were run in the University of Luxembourg HPC platform using sbatch. 
	One script per microscope run was used to run them in parallel in the HPC. Settings across stainings were the same. 
	Information of the section per microscope run can be found in the "Organoid_Selection_01.txt" file
	
